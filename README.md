# IcsMerge

A web application to bundle ics files or web sources into a combined ics files,
and making them available to calendars and applications that can poll by urls.

## Running

To just run the code of this project, you can download a nodejs version of it, can be found here: [`build-nodejs.zip`](https://gitlab.com/jaavander/icsmerge/-/jobs/artifacts/main/browse?job=build-nodejs).  
The folder contained therein can be executed with nodejs.

### Quickstart:
```sh
# Downloading & unzipping icsmerge
curl -L https://gitlab.com/jaavander/icsmerge/-/jobs/artifacts/main/download?job=build-nodejs > build.zip
unzip build.zip
# Running the server
node build
```

> ℹ️  This quickstart does assume `curl`, `unzip` and `node` are available.  
> The node version should be at least 16.

### Configuration:

You can further customize the behavior of icsmerge using env variables. 

In any build of icsmerge, the following env variables can be set: 
| Variable | Description | Default |
| ---      | ---         | ---     |
| `MAX_SOURCES` | maximum number of sources that may be in the same request | 10 |
| `MAX_CONTENT_LENGTH`| maximum allowed content size. Only checked for sources that report content-size | 1000000 (1mb)
| `HTTP_SOURCES_ALLOWED` | if set as any value other then "", it will be allowed to rewrite webcal urls into http urls if the https equivalent can not be found | "" |


For the nodejs build, there are also the `HOST` and `PORT` variables, to configure on which host and port the app binds.

## Developing

If you have corrections or improvements, make a fork, commit your changes there, and open a merge request.

If you find a bug, or want to make a feature request, open an issue.

To start a local development version of the app, 
ensure nodejs & npm are installed, and run 
```bash
npm install
npm run dev
```