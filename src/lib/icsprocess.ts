import { env } from "$env/dynamic/private";
import { error } from "@sveltejs/kit";

const {
  MAX_CONTENT_LENGTH,
  HTTP_SOURCES_ALLOWED,
} = env;

export interface ICSParts {
  head: string[];
  tail: string[];
  events: string[][];
}

export function parseIcsContent(fileContent: string): ICSParts {
  const lines = fileContent.split("\n").map(line => line.trimEnd());
  const eventStart = lines.indexOf("BEGIN:VEVENT");
  const eventEnd = lines.lastIndexOf("END:VEVENT");
  const events: string[][] = [];
  let currentEvent: string[] = [];
  for (var line of lines.slice(eventStart, eventEnd + 1)) {
    if (line === "BEGIN:VEVENT")
      currentEvent = [];
    else if (line === "END:VEVENT")
      events.push(currentEvent);
    currentEvent.push(line);
  }
  return {
    head: lines.slice(0, eventStart),
    tail: lines.slice(eventEnd + 1),
    events,
  };
}

const maxContentLength = parseInt(MAX_CONTENT_LENGTH ?? "50000");
function checkResponseLength(response: Response) {
  const lengthStr = response.headers.get("content-length");
  const length = parseInt(lengthStr ?? "0");
  if (length > maxContentLength) {
    throw error(400, `File "${response.url}" had content-length ${length}, which exceeds maximum of ${maxContentLength}`);
  }
}

export function filterSummary(events: string[][], terms: (string | null)[]): string[][] {
  const activeTerms = terms.filter(v => v) as string[];
  if (activeTerms.length === 0)
    return events;
  return events.filter((event) => {
    const summary = event.find(x => x.startsWith("SUMMARY"));
    return !summary || activeTerms.every(filter => summary.substring(8).includes(filter));
  })
}

export async function processSource(source: URL, signal: AbortSignal): Promise<ICSParts> {
  if (source.protocol === "webcal")
    source.protocol = "https";
  let response: Response;
  try {
    response = await fetch(source, { signal });
    checkResponseLength(response)
  } catch (error) {
    if (error instanceof Response && HTTP_SOURCES_ALLOWED) {
      source.protocol = "http";
      response = await fetch(source, { signal });
      checkResponseLength(response)
    } else
      throw error;
  }
  const text = await response.text();
  return parseIcsContent(text);
}

export async function processMergeRequest(
  sources: string[],
  filter: string | null,
  filters: (string | null)[],
): Promise<string> {
  const controller = new AbortController();
  let lineSets: ICSParts[];
  try {
    lineSets = await Promise.all(
      sources.map(source => processSource(
        new URL(source),
        controller.signal,
      ))
    );
  } finally {
    controller.abort();
  }
  const { head } = lineSets[0];
  const usedEvents: string[] = lineSets
    .map(({ events }, index) => filterSummary(events, [filter, filters[index]]))
    .flat(2);
  const { tail } = lineSets.at(-1)!;
  return head.concat(usedEvents).concat(tail).join("\n");
}