import { error, text, type RequestHandler } from "@sveltejs/kit";
import { processMergeRequest, processSource, type ICSParts } from "$lib/icsprocess";
import { env } from "$env/dynamic/private";

const {
  MAX_SOURCES,
} = env;

const maxSources = parseInt(MAX_SOURCES || "10")!;
export const GET: RequestHandler = async function ({ url }) {
  const sources = url.searchParams.getAll("source");
  if (sources.length === 0) {
    throw error(400, "No 'source' parameters found");
  }
  if (sources.length > maxSources) {
    throw error(400, `More then ${maxSources} specified`);
  }
  if (new Set(sources).size !== sources.length) {
    throw error(400, `Identical source used multiple times`)
  }
  const filter = url.searchParams.get("filter");
  const filters = sources.map((_, i) => url.searchParams.get(`filter${i}`));
  const outputContent = await processMergeRequest(sources, filter, filters);
  return text(outputContent, {
    headers: { "content-type": "text/calendar" }
  })
};