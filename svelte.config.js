import preprocess from 'svelte-preprocess';

const {
	SVELTE_ADAPTER_PACKAGE
} = process.env;

const adapter = SVELTE_ADAPTER_PACKAGE
	? await import(SVELTE_ADAPTER_PACKAGE)
	: undefined;

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: preprocess(),
	kit: {
		adapter: adapter?.default(),
	}
};

export default config;
